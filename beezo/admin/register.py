import os
import platform
import argparse
import ConfigParser
import requests

def main():
    parser = argparse.ArgumentParser(description='Register host with Beezo.')
    parser.add_argument('--conf', required=True, help='configuration file')
    args = parser.parse_args()
    fileconf = args.conf

    if not os.path.isfile(fileconf):
        print('Error. Configuration file not found: %s' % (fileconf))
        exit(1)

    configParser = ConfigParser.ConfigParser(allow_no_value=True)
    configParser.read(fileconf)
    host_uuid = configParser.get('beezo', 'host_uuid')
    token = configParser.get('beezo', 'token')
    alias = None

    if len(token) == 0:
        token = raw_input('Please, inform your BzAgent token: ')
        configParser.set('beezo', 'token', token)

    if len(host_uuid) > 0:
        print('Host already registered as: %s' % (host_uuid))
        exit()

    alias = raw_input('Please, inform an alias for this host: ')
    if len(alias) == 0:
        alias = platform.platform()
        print('Alias not informed, continue assuming: %s' % (alias))

    configParser.set('beezo', 'alias', alias)

    url = 'http://192.168.15.5:8080/api/v1/hosts/new'

    payload = {
        'token': token,
        'os_type': 'LINUX',
        'name': alias
    }

    response = requests.request('POST', url, json=payload)

    if response.status_code == 200:
        result = response.json()
        host_uuid = result.get('host')
        configParser.set('beezo', '; WARNING Never change the host_uuid to avoid conflict.')
        configParser.set('beezo', 'host_uuid', host_uuid)
        print('Host registered as: %s' % (host_uuid))
    elif response.status_code == 401:
        print('Invalid token')
        exit()
    else:
        print('An error occurred during registration')
        exit()

    with open(fileconf, 'wb') as configfile:
        configParser.write(configfile)

if __name__ == "__main__":
    main()
