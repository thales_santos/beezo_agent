import sys
import os
import time
import logging
import itertools
import argparse
import ConfigParser
import requests
from operator import itemgetter
from beezo.collectors.memory import get_meminfo
from beezo.collectors.cpu import get_cpu_stats, calculate_cpu_usage
from beezo.collectors.disks import disk_usage
from beezo.collectors.network import get_nics, get_ip_addr
from beezo.collectors.process import get_pids, get_process_info

AGENT_VERSION = '1.0'

class Agent(object):

    def __init__(self, host, token):
        self.__host = host
        self.__token = token
        self.previous_cpu_sample = None
        self.previous_processes = None
        self.previous_nics = None
        self.page_size = os.sysconf("SC_PAGE_SIZE")
        self.__payload = { 'version': AGENT_VERSION }

    def _get_previous_process(self, proc_id):
        tmp = [p for p in self.previous_processes if p['pid'] == proc_id]
        if len(tmp) == 0:
            return None
        else:
            return tmp[0]

    def _get_previous_nic(self, name):
        tmp = [nic for nic in self.previous_nics if nic['name'] == name]
        if len(tmp) == 0:
            return None
        else:
            return tmp[0]

    def _group_processes_by(self, pids, group_key):
        _grouped_pids = []
        pids = sorted(pids, key=lambda pid_info: pid_info[group_key])

        for key, items in itertools.groupby(pids, key=itemgetter(group_key)):
            total_mem_usage = 0
            total_cpu_usage = 0

            for i in items:
                total_mem_usage = total_mem_usage + i['memory']
                total_cpu_usage = total_cpu_usage + i['cpu']
                comm = i['comm']
                pgrp = i['pgrp']

            _grouped_pids.append({
                'pgrp': pgrp,
                'comm': comm,
                'memory': total_mem_usage,
                'cpu': total_cpu_usage
            })

        return _grouped_pids

    def _collect_memory(self):
        meminfo = get_meminfo()
        log.info('[MEMORY]')
        log.info('Total Memory: %s bytes' % meminfo.get('MemTotal'))
        log.info('Memory Available: %s bytes' % meminfo.get('MemAvailable'))
        self.__payload['memory'] = {
            'total': meminfo.get('MemTotal'),
            'available': meminfo.get('MemAvailable'),
            'swap_total': meminfo.get('SwapTotal'),
            'swap_free': meminfo.get('SwapFree')
        }

    def _collect_disk_usage(self):
        log.info('[DISK]')
        stats = disk_usage('/')
        usage = (stats['free'] * 100.0) / stats['total']
        log.info("Total disk usage: %0.2f" % usage)
        self.__payload['disk'] = {
            'total': stats['total'],
            'free': stats['free']
        }

    def _collect_nics(self):
        nics = get_nics()
        nic_infos = []
        if self.previous_nics is not None:
            log.info('[NETWORK]')
            for nic in nics:
                previous_nic = self._get_previous_nic(nic['name'])

                if previous_nic is not None:
                    ip_addr = get_ip_addr(nic['name'])
                    log.info('Network Interface: %s' % nic['name'])
                    log.info('RX Bytes: %d' % (nic['rx']['bytes'] - previous_nic['rx']['bytes']))
                    log.info('RX Packets: %d' % (nic['rx']['packets'] - previous_nic['rx']['packets']))
                    log.info('TX Bytes: %d' % (nic['tx']['bytes'] - previous_nic['tx']['bytes']))
                    log.info('TX Packets: %d' % (nic['tx']['packets'] - previous_nic['tx']['packets']))
                    log.info('IP Address: %s' % ip_addr)
                    nic_infos.append({
                        'name': nic['name'],
                        'ip_addr': ip_addr,
                        'rx': {
                            'bytes': nic['rx']['bytes'] - previous_nic['rx']['bytes'],
                            'packets': nic['rx']['packets'] - previous_nic['rx']['packets'],
                        },
                        'tx': {
                            'bytes': nic['tx']['bytes'] - previous_nic['tx']['bytes'],
                            'packets': nic['tx']['packets'] - previous_nic['tx']['packets']
                        }
                    })

        self.__payload['networks'] = nic_infos
        self.previous_nics = list(nics)

    def _collect_cpu_stats(self):
        cpu_sample = get_cpu_stats()

        if self.previous_cpu_sample is not None:
            log.info('[CPU]')
            cpu_usage = calculate_cpu_usage(cpu_sample.get('cpu'), self.previous_cpu_sample.get('cpu'))
            log.info('CPU Usage: %0.2f' % cpu_usage)
            self.__payload['cpu'] = {
                'usage': round(cpu_usage, 2)
            }

        self.__payload['processes'] = []

        pids_info = []

        for pid in get_pids():
            try:
                process_info = get_process_info(pid)
                if process_info is not None:
                    pids_info.append(process_info)
            except IOError:
                continue

        if self.previous_processes is not None:
            log.info('[PROCESS]')
            total_cpu_time = sum(cpu_sample.get('cpu').values()) - sum(self.previous_cpu_sample.get('cpu').values())

            pids = []

            for process in pids_info:
                previous_pid = self._get_previous_process(process['pid'])

                if previous_pid is not None:
                    prt = previous_pid['utime'] + previous_pid['stime']
                    crt = process['utime'] + process['stime']
                    elapsed_time = crt - prt

                    try:
                        process_usage = round(((elapsed_time * 100.0) / total_cpu_time), 2)
                    except ZeroDivisionError:
                        process_usage = 0

                    pids.append({
                        'pid': process['pid'],
                        'comm': process['comm'],
                        'pgrp': process['pgrp'],
                        'memory': (process['rss'] * self.page_size),
                        'cpu': process_usage
                    })

            grouped_pids = self._group_processes_by(pids, 'comm')

            # Order processes by memory usage
            sorted_by_mem = sorted(grouped_pids, key=lambda pid_info: pid_info['memory'])

            for p in sorted_by_mem[-30:]:
                log.info('[%s] %s:\t%d Bytes, %0.2f' % (p['pgrp'], p['comm'], p['memory'], p['cpu']))
                self.__payload['processes'].append(p)

        self.previous_processes = list(pids_info)
        self.previous_cpu_sample = dict(cpu_sample)

    def _send_payload(self):
        url = 'http://192.168.15.5:8080/api/v1/hosts/%s/metrics' % (self.__host)
        headers = {'authorization': self.__token}
        response = requests.request('POST', url, headers=headers, json=self.__payload)

        if response.status_code == 200:
            log.info('Information sent successfully')

        elif response.status_code == 401:
            msg = 'Invalid token. Please check your configuration file.'
            print(msg)
            log.error(msg)
            exit(1)

        elif (response.status_code)/100 == 4:
            msg = 'A client error has occurred when sending data: %s\nExiting...' % (response.text)
            print(msg)
            log.error(msg)
            exit(1)

        elif (response.status_code)/100 == 5:
            msg = 'A server error has occurred when sending data. Retry will happen automatically...'
            log.info(msg)

    def run(self):
        log.info('Starting Beezo Monitoring Agent (version=%s) for host %s' % (AGENT_VERSION, self.__host))
        while True:
            self._collect_memory()
            self._collect_disk_usage()
            self._collect_nics()
            self._collect_cpu_stats()
            if self.previous_processes is not None:
                self._send_payload()
            time.sleep(60)

def main():
    parser = argparse.ArgumentParser(description='Beezo host monitoring agent.')
    parser.add_argument('--conf', required=True, help='configuration file')
    args = parser.parse_args()
    fileconf = args.conf

    if not os.path.isfile(fileconf):
        print('Error. Configuration file not found: %s' % (fileconf))
        exit(1)

    configParser = ConfigParser.ConfigParser(allow_no_value=True)
    configParser.read(fileconf)
    host = configParser.get('beezo', 'host_uuid')
    token = configParser.get('beezo', 'token')
    log_file = configParser.get('beezo', 'log_file')

    logging.basicConfig(filename=log_file, format='%(asctime)s %(message)s',level=logging.DEBUG)
    global log
    log = logging.getLogger()

    if len(token) == 0:
        msg = 'Token not found in the configuration file. Please, run the beezo-register tool again.'
        print(msg)
        log.error
        exit(1)

    agent = Agent(host=host, token=token)
    agent.run()

if __name__ == "__main__":
    main()
