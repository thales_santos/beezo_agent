import os


def get_pids():
    return [pid for pid in os.listdir('/proc') if pid.isdigit()]


def get_process_info(pid):
    try:
        with open(os.path.join('/proc/', pid, 'stat'), 'r') as pidfile:
            procstat = pidfile.readline().split()
            # get utime from /proc/<pid>/stat, 14 item
            stat = {
                'pid': procstat[0],
                'comm': procstat[1].replace('(', '').replace(')', ''),
                'ppid': int(procstat[3]),
                'pgrp': int(procstat[4]),
                'session': int(procstat[5]),
                'utime': int(procstat[13]),
                # get stime from proc/<pid>/stat, 15 item
                'stime': int(procstat[14]),
                'rss': int(procstat[23]) # use rss over vsize
                # count total process used time
                #proctotal = int(utime) + int(stime)
            }
            return stat
    except IOError as e:
        # process not found
        return None


if __name__ == '__main__':
    import itertools
    from operator import itemgetter

    pids = get_pids()
    pids_info = []

    for pid in pids:
        try:
            pids_info.append(get_process_info(pid))
        except IOError: # proc has already terminated
            continue

    page_size = os.sysconf("SC_PAGE_SIZE")

    #for r in sorted(pids_info, key=lambda pid_info: pid_info['rss']):
        #print r
        #print "%s:\t\t\t%d KB" % (r['comm'], (r['rss'] * page_size)/1024)

    pids = sorted(pids_info, key=lambda pid_info: pid_info['pgrp'])

    for pgrp, grouped_pids in itertools.groupby(pids, key=itemgetter('pgrp')):
        print pgrp
        for pid in grouped_pids:
            print pid #i.get('pgrp')
