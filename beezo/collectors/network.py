import socket
import fcntl
import struct

def get_ip_addr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


def get_nics(exclude_loopback=True):
    with open('/proc/net/dev', 'r') as f:
        nics = []
        content = f.readlines()[2:]
        for line in content:
            sl = line.split()
            ifname = sl[0].replace(':', '')

            if ifname != 'lo':
                nics.append({
                    'name': ifname,
                    'rx': {
                        'bytes': int(sl[1]),
                        'packets': int(sl[2]),
                        'errs': int(sl[3]),
                        'drops': int(sl[4]),
                        'fifo': int(sl[5]),
                        'frame': int(sl[6]),
                        'compressed': int(sl[7]),
                        'multicast': int(sl[8])
                    },
                    'tx': {
                        'bytes': int(sl[9]),
                        'packets': int(sl[10]),
                        'errs': int(sl[11]),
                        'drops': int(sl[12]),
                        'fifo': int(sl[13]),
                        'colls': int(sl[14]),
                        'carrier': int(sl[15]),
                        'compressed': int(sl[16])
                    }
                })

        return nics


if __name__ == '__main__':

    for nic in get_nics():
        print 'Network Interface: %s' % nic['name']
        print 'RX Bytes: %d' % nic['rx']['bytes']
        print 'RX Packets: %d' % nic['rx']['packets']
        print 'TX Bytes: %d' % nic['tx']['bytes']
        print 'TX Packets: %d' % nic['tx']['packets']
        print 'IP Address: %s' % get_ip_addr(nic['name'])
        print
