import os

SYS_BLOCK_DIR = '/sys/block'

def disk_usage(path):
    """Return disk usage statistics about the given path.

    Returned value is a dictiony containing 'free' and 'total' spaces, in bytes.
    """
    st = os.statvfs(path)
    return {
        'free': st.f_bavail * st.f_frsize,
        'total': st.f_blocks * st.f_frsize
    }


def get_block_device_type(block_dev):
    dev_path = os.path.join(SYS_BLOCK_DIR, block_dev, 'dev')
    print os.path.isfile(dev_path)

    with open(dev_path, 'r') as dev_file:
        sl = dev_file.readline().replace('\n', '').split(':')
        major = -1

        if len(sl) == 2:
            major = int(sl[0])
            minor = int(sl[1])

        print major
        print minor
        print 
        # get utime from /proc/<pid>/stat, 14 item


def get_block_devices():
    return os.listdir(SYS_BLOCK_DIR)


if __name__ == '__main__':
    stats = disk_usage('/')
    usage = (stats['free'] * 100.0) / stats['total']
    print "Total disk usage: %0.2f" % (usage)

    block_devices = get_block_devices()

    for block_dev in block_devices:
        get_block_device_type(block_dev)
