def get_meminfo():
    with open('/proc/meminfo', 'r') as f:
        metrics = {}
        content = f.readlines()
        for line in content:
            sl = line.split()
            name = sl[0].replace(':', '')
            metrics[name] = sl[1]

        return metrics


if __name__ == '__main__':
    meminfo = get_meminfo()
    print "Total Memory: %s bytes" % meminfo['MemTotal']
    print "Memory Available: %s bytes" % meminfo['MemAvailable']


