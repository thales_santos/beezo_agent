import time

PROC_STAT = '/proc/stat'

def get_cpu_stats():
    """
    Read the /proc/stat OS file and parses it

    1st column : user = normal processes executing in user mode
    2nd column : nice = niced processes executing in user mode
    3rd column : system = processes executing in kernel mode
    4th column : idle = twiddling thumbs
    5th column : iowait = waiting for I/O to complete
    6th column : irq = servicing interrupts
    7th column : softirq = servicing softirqs

    Since Linux 2.6.11, there is an 8th column called steal
    Since Linux 2.6.24, there is a 9th column called guest
    Refer to http://www.linuxhowtos.org/manpages/5/proc.htm

    """
    with open(PROC_STAT, 'r') as f:
        cpu_stats = {}
        content = [line for line in f.readlines() if line.startswith('cpu')]
        for line in content:
            sl = line.split()
            cpu = {
                'user': int(sl[1]),
                'nice': int(sl[2]),
                'system': int(sl[3]),
                'idle': int(sl[4]),
                'iowait': int(sl[5]),
                'irq': int(sl[6]),
                'softirq': int(sl[7])
            }

            if len(sl) > 8:
                cpu['steal'] = int(sl[8])

            if len(sl) > 9:
                cpu['guest'] = int(sl[9])

            cpu_stats[sl[0]] = cpu

        return cpu_stats


def calculate_cpu_usage(sample1, sample2):
    sample1_total = sum(sample1.values())
    sample2_total = sum(sample2.values())
    total = sample2_total - sample1_total
    idle = sample2['idle'] - sample1['idle']
    usage = (total - idle) * 100.0 / total
    return usage


if __name__ == '__main__':
    sample_1 = get_cpu_stats()
    time.sleep(60)
    sample_2 = get_cpu_stats()
    print calculate_cpu_usage(sample_1.get('cpu'), sample_2.get('cpu'))
