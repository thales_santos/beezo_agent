from setuptools import setup

setup(name='beezo',
      version='0.1',
      description='Beezo Agent',
      author='Thales Angelino',
      author_email='thales.angelino@gmail.com',
      packages=['beezo', 'beezo.collectors', 'beezo.admin'],
      install_requires=[
          'requests',
      ],
      scripts=['bin/beezo-agent', 'bin/beezo-register'],
      zip_safe=False)
