#!/bin/sh

CONF_DIR=/etc/bzagent
LOG_DIR=/var/log/beezo

CONFIG_FILE=$CONF_DIR/beezo.conf
LOG_FILE=$LOG_DIR/bzagent.log
LOG_LEVEL=debug

mkdir -p $CONF_DIR
mkdir -p $LOG_DIR

[ -f $LOG_FILE ] && echo "BzAgent log file already exist" || touch $LOG_FILE

CONF="[beezo]\n\n; WARNING Never change the host_uuid to avoid conflict.\n\nhost_uuid=\n\n; Inform the secret token\ntoken=\n\nlog_file=$LOG_FILE\n\n; log_level=$LOG_LEVEL"

[ -f $CONFIG_FILE ] && echo "BzAgent configuration file already exist" || echo $CONF >> $CONFIG_FILE
